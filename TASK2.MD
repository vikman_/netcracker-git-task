10. Вызвать комманды diff, status:
  * git diff
  * git status
11. Закоммитить изменения, снова проверить статус:
  * git commit -a -m "updated TASK1.MD"
  * git status
12. Запушить на GitLab:
  * git push -u gitlab

# Task 2

1. ===//===
2. Отвести бранч develop, добавить туда TASK2.MD, залить его локально и удалённо:
  * git branch develop
  * git checkout develop
  * git add TASK2.MD
  * git commit -m "add TASK2.MD to develop branch"
  * git push -u gitlab
3. Добавить в конец файла TASK1.MD строку, залить локально и удалённо:
  * git commit -a -m "updated TASK1.MD"
  * git push -u gitlab
4. Переключиться назад на основной бранч:
  * git switch master
5. Добавить в конец файла TASK1.MD строку, залить локально и удалённо:
  * git commit -a -m "updated TASK1.MD"
  * git push -u gitlab
6. Выполнить мерж из ветки develop в основную ветку. Разрешить конфликт в пользу основной ветки:
  * git merge develop
  * git status
  * git commit -a -m "merge"
  * git push -u gitlab
7. Посмотреть лог проделанных изменений:
  * git log

PS Я буду делать задание 3
