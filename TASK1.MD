# Task 1

1. Зарегистрировать аккаунт на GitLab
2. Создать репозиторий
3. Установить git на локальную машину
4. Инициализировать локальный репозиторий: 
  * git init
5. Связать локальный репозиторий с удалённым:
  * git remote add gitlab https://gitlab.com/vikman_/netcracker-git-task.git
6. ===//===
7. Закоммитить файл в локальный репозиторий:
  * git add TASK1.MD
  * git commit -m "init"
8. Запушить локальный репозиторий на GitLab:
  * git push -u gitlab

Задание успешно выполнено!:)
